import React, {Component} from 'react';
import Login from './Login';
import Signup from './Signup';
import {Button, Card, Row, Col} from "antd";
import "antd/dist/antd.css";

class Auth extends Component{
    state = {
        isSignup: false
    }

    buttonClickedHandler = (prevState) => {
        this.setState({isSignup: !prevState.isSignup});
    }
    render(){
        var renderedComponent = <Login />;
        if(this.state.isSignup === true){
            renderedComponent = <Signup />;
        }
        return(
            <div>
                <Row>
                    <Col span={6}/>
                    <Col span={12}>
                        <Card title={this.state.isSignup ? 'SIGN-UP' : 'LOGIN'} >
                            {renderedComponent}
                            <Button onClick={() => this.buttonClickedHandler(this.state)}>
                                Or {this.state.isSignup ? 'login' : 'register now'}!
                            </Button>
                        </Card>
                    </Col>
                    <Col span={6}/>
                </Row>
            </div>
        );
    }
}

export default Auth;