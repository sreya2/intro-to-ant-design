import React from "react";
import ReactDOM from "react-dom";
// import Todo from "./todo";
import Auth from './Auth/Auth';

import "./styles.css";

function App() {
  return (
    <div className="App">
      <Auth />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);